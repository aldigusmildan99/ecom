﻿
using ecom.Core.Dtos.KategoriProduk;
using EN = ecom.Core.Entities;
using ecom.Core.InterFaces;
using Microsoft.EntityFrameworkCore;

namespace ecom.Core.Services.KategoriProduk;

public class KategoriProdukServices : IKategoriProdukServices
{
    private readonly ApplicationDbContext _context;

    public KategoriProdukServices(ApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<IEnumerable<EN::KategoriProduk>> Cari(CariKategoriProdukDto cariKategoriProdukDto, int page = 1, int perPage = 10)
    {
        IQueryable<EN::KategoriProduk> kategoriProdukQuery = _context.kategoriProduks.Include(i => i.Produks);

        kategoriProdukQuery = cariKategoriProdukDto.CariNama(kategoriProdukQuery);

        var result = await kategoriProdukQuery
        .Skip((page - 1) * perPage).Take(perPage)
        .ToListAsync();

        return result;
    }

    public async Task<EN::KategoriProduk> Create(KategoriProdukCreateDto kategoriProdukUpdateDto)
    {
        var kategoriProduk = new EN::KategoriProduk
        {
            Nama = kategoriProdukUpdateDto.Nama,
        };

        await this._context.kategoriProduks.AddAsync(kategoriProduk);
        await this._context.SaveChangesAsync();

        return kategoriProduk;
    }

    public async Task<EN::KategoriProduk> Delete(int id)
    {
        var kategoriProduk = await this.GetOne(id);
        if (kategoriProduk == null) return null;

        _context.kategoriProduks.Remove(kategoriProduk);

        _context.SaveChanges();
        return kategoriProduk;
    }

    public async Task<IEnumerable<EN::KategoriProduk>> GetAll()
    {
        return await _context.kategoriProduks.Include(i => i.Produks).OrderBy(i => i.Nama).ToListAsync();
    }

    public async Task<EN::KategoriProduk> GetOne(int id)
    {
        return await _context.kategoriProduks.FindAsync(id);
    }

    public async Task<EN::KategoriProduk> Update(int id, KategoriProdukUpdateDto kategoriProdukUpdateDto)
    {
        var kategoriProduk = await this.GetOne(id);
        if (kategoriProduk == null) return null;

        kategoriProduk.Nama = kategoriProdukUpdateDto.Nama;

        await _context.SaveChangesAsync();

        return kategoriProduk;

    }
}
