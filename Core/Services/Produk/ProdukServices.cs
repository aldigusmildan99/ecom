﻿
using ecom.Core.Dtos.Produk;
using EN = ecom.Core.Entities;
using ecom.Core.InterFaces;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;

namespace ecom.Core.Services.Produk;

public class ProdukServices : IProdukServices
{
    private readonly ApplicationDbContext _context;

    public ProdukServices(ApplicationDbContext context)
    {
        _context = context;
    }

    private void cariStok(ref IQueryable<EN::Produk> obj, int val){
       obj = obj.Where(i=>i.Stok == val);
    }

    public async Task<IEnumerable<EN::Produk>> Cari(CariProdukDto cariProdukDto, int page=1, int perPage=10)
    {
       IQueryable<EN::Produk> produkQuery = _context.produks.Include(i=>i.KategoriProduk);

       produkQuery = cariProdukDto.CariNama(produkQuery);
       produkQuery = cariProdukDto.CariStok(produkQuery);
       produkQuery = cariProdukDto.CariNamaKategori(produkQuery);
    
       var result = await produkQuery
       .Skip((page-1) * perPage).Take(perPage)
       .ToListAsync();

       return result;
    }

    public async Task<EN::Produk> Create(ProdukCreateDto produkDto)
    {
        var kategoriProduk = await this._context.kategoriProduks.FindAsync(produkDto.KategoriProdukId);

        var produk = new EN::Produk{
            Nama = produkDto.Nama,
            Stok = produkDto.Stok,
            Gambar = produkDto.Gambar,
            KategoriProduk = kategoriProduk
        };

        await this._context.produks.AddAsync(produk);
        await this._context.SaveChangesAsync();

        return produk;
    }

    public async Task<EN::Produk> Delete(int id)
    {
        var produk = await this.GetOne(id);
        if(produk == null) return null;

        _context.produks.Remove(produk);

        _context.SaveChanges();
        return produk;
    }

    public async Task<IEnumerable<EN::Produk>> GetAll()
    {
        return await _context.produks.Include(i=>i.KategoriProduk).OrderBy(i=>i.Nama).ToListAsync();
    }

    public async Task<EN::Produk> GetOne(int id)
    {
        return await _context.produks.FindAsync(id);
    }

    public async Task<EN::Produk> Update(int id, ProdukUpdateDto produkDto)
    {
        var produk = await this.GetOne(id);
        if(produk == null) return null;

        var kategoriProduk = await this._context.kategoriProduks.FindAsync(produkDto.KategoriProdukId);

        produk.Stok = produkDto.Stok;
        produk.Nama = produkDto.Nama;
        produk.Gambar = produkDto.Gambar;
        produk.KategoriProduk = kategoriProduk;
        
        await _context.SaveChangesAsync();

        return produk;
        
    }
}
