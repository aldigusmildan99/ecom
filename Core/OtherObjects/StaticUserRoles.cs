﻿namespace ecom;

public class StaticUserRoles
{
    public const string ADMIN = "ADMIN";
    public const string SELLER = "SELLER";
    public const string USER = "USER";
}
