﻿using ecom.Core.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace ecom;

public class ApplicationDbContext: IdentityDbContext<ApplicationUser>
{
    private readonly IConfiguration Configuration;
    private string? connStr;

    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options, IConfiguration configuration) : base(options)
    {
        Configuration = configuration;
        connStr = Configuration.GetConnectionString("default");
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseMySql(this.connStr, ServerVersion.AutoDetect(this.connStr));
    }

    public DbSet<Produk> produks {get;set;} 
    public DbSet<KategoriProduk> kategoriProduks {get;set;}
}
