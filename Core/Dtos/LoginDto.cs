﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace ecom;

public class LoginDto
{

    [Required(ErrorMessage = "Username wajib")]
    public string UserName {get;set;}

    [Required(ErrorMessage = "Password wajib")]
    public string Password {get;set;}
}
