﻿using System.ComponentModel.DataAnnotations;

namespace ecom.Core.Dtos.KategoriProduk;

public class KategoriProdukCreateDto
{
    [Required(ErrorMessage = "Nama Kategori wajib isi")]
    public string Nama {get;set;}
}
