using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using Microsoft.IdentityModel.Tokens;
using EN = ecom.Core.Entities;
namespace ecom.Core.Dtos.KategoriProduk;

public class CariKategoriProdukDto
{
    public string? Nama {get;set;} = null;


    public IQueryable<EN::KategoriProduk> CariNama(IQueryable<EN::KategoriProduk> produkQuery){
        if(!this.Nama.IsNullOrEmpty()){
            produkQuery = produkQuery.Where(i => i.Nama.Contains(this.Nama));
        }
        return produkQuery;
    }
}
