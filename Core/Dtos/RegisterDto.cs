﻿using System.ComponentModel.DataAnnotations;

namespace ecom;

public class RegisterDto
{
    [Required(ErrorMessage = "Firstname wajib")]
    public string FirstName {get;set;}

    [Required(ErrorMessage = "LastName wajib")]
    public string LastName {get;set;}

    [Required(ErrorMessage = "Username wajib")]
    public string UserName {get;set;}

    [Required(ErrorMessage = "Email wajib")]
    public string Email {get;set;}

    [Required(ErrorMessage = "Password wajib")]
    public string Password {get;set;}
}
