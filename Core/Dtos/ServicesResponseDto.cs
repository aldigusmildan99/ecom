﻿namespace ecom;

public class ServicesResponseDto
{
    public int code {get;set;}
    public bool status {get;set;}
    public string? message {get;set;}
    public object? data {get;set;}

    public ServicesResponseDto(int code, bool status, string? message="", object? data=null)
    {
        this.code = code;
        this.status = status;
        this.message = message;
        this.data = data;
    }
}
