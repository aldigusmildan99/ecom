using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using EN = ecom.Core.Entities;
using Microsoft.IdentityModel.Tokens;

namespace ecom.Core.Dtos.Produk;

public class CariProdukDto
{
    public string? Nama {get;set;} = null;

    public int? Stok {get;set;} =null;

    public string? Gambar {get;set;} = null;

    public int? KategoriProdukId {get;set;} = null;

    public string? kategoriproduk_nama {get;set;} = null;


    public IQueryable<EN::Produk> CariNama(IQueryable<EN::Produk> produkQuery){
        if(!this.Nama.IsNullOrEmpty()){
            produkQuery = produkQuery.Where(i => i.Nama.Contains(this.Nama));
        }
        return produkQuery;
    }
    public IQueryable<EN::Produk> CariStok(IQueryable<EN::Produk> produkQuery){
        if(this.Stok != null){
            produkQuery = produkQuery.Where(i => i.Stok == this.Stok);
        }
        return produkQuery;
    }

    public IQueryable<EN::Produk> CariNamaKategori(IQueryable<EN::Produk> produkQuery){
        if(!this.kategoriproduk_nama.IsNullOrEmpty()){
            produkQuery = produkQuery.Where(i => i.KategoriProduk.Nama.Contains(this.kategoriproduk_nama));
        }
        return produkQuery;
    }
}
