﻿using System.ComponentModel.DataAnnotations;

namespace ecom.Core.Dtos.Produk;

public class ProdukUpdateDto
{
    [Required(ErrorMessage = "Nama wajib isi")]
    public string Nama {get;set;}

    [
        Required(ErrorMessage = "Stok wajib isi"), 
        Range(0,Int64.MaxValue, ErrorMessage = "Stok harus berupa angka")
    ]
    public int Stok {get;set;}

    public string? Gambar {get;set;} = null;

    public int? KategoriProdukId {get;set;} = null;
}
