﻿using ecom.Core.Dtos.Produk;
using ecom.Core.Entities;

namespace ecom.Core.InterFaces;

public interface IProdukServices
{
    public Task<IEnumerable<Produk>>  GetAll();
    public Task<Produk> GetOne(int id);
    public Task<Produk> Create(ProdukCreateDto produk);
    public Task<Produk> Update(int id, ProdukUpdateDto produk);
    public Task<Produk> Delete(int id);
    public Task<IEnumerable<Produk>> Cari(CariProdukDto cariProdukDto, int page=1, int perPage=10);
}
