﻿using ecom.Core.Dtos.KategoriProduk;
using ecom.Core.Dtos.Produk;
using ecom.Core.Entities;
namespace ecom.Core.InterFaces;

public interface IKategoriProdukServices
{
    public Task<IEnumerable<KategoriProduk>>  GetAll();
    public Task<KategoriProduk> GetOne(int id);
    public Task<KategoriProduk> Create(KategoriProdukCreateDto produk);
    public Task<KategoriProduk> Update(int id, KategoriProdukUpdateDto produk);
    public Task<KategoriProduk> Delete(int id);
    public Task<IEnumerable<KategoriProduk>> Cari(CariKategoriProdukDto cariProdukDto, int page=1, int perPage=10);
}
