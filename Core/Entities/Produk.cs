﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using Microsoft.IdentityModel.Tokens;

namespace ecom.Core.Entities;

[Table("produk")]
public class Produk
{
    public int Id { get; set; }
    
    [Required]
    public string Nama {get;set;}

    [DefaultValue(0)]
    public int Stok {get;set;}

    public string? Gambar {get;set;} = null;

    public KategoriProduk? KategoriProduk {get;set;} = null;
}
