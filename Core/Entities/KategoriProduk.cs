﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace ecom.Core.Entities;

[Table("kategori_produk")]
public class KategoriProduk
{

    public int Id {get;set;}

    [Required(ErrorMessage = "Nama wajib diisi")]
    public string Nama {get;set;}

    [JsonIgnore]
    public List<Produk>? Produks {get;}
}
