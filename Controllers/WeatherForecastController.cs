using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace ecom.Controllers;

[ApiController]
[Route("[controller]")]
public class WeatherForecastController : ControllerBase
{
    private UserManager<ApplicationUser> _orang;
    private static readonly string[] Summaries = new[]
    {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

    private readonly ILogger<WeatherForecastController> _logger;

    public WeatherForecastController(ILogger<WeatherForecastController> logger, UserManager<ApplicationUser> orang)
    {
        _logger = logger;
        _orang = orang;
    }

    [HttpGet(Name = "GetWeatherForecast")]
    // [Authorize]
    public IActionResult Get()
    {
        _logger.LogTrace("tanpa kudu login");
        _logger.LogDebug("tanpa kudu login");
        _logger.LogInformation("tanpa kudu login");
        _logger.LogWarning("tanpa kudu login");
        _logger.LogCritical("tanpa kudu login");
        _logger.LogError("tanpa kudu login");
       return Ok(Summaries);
    }

    [HttpGet, Route("getUser"), Authorize(Roles = StaticUserRoles.USER)]
    public async Task<IActionResult> GetUser(){
        return Ok(Summaries);
    }

    [HttpGet, Route("getAdmin"), Authorize(Roles = StaticUserRoles.ADMIN)]
    public async Task<IActionResult> GetAdmin(){
        return Ok(Summaries);
    }

    [HttpGet, Route("getAdminOrUser"), Authorize(Roles = "ADMIN,USER")]
    public async Task<IActionResult> GetUserOrAdmin(){
        _logger.LogError("Ini harus role user atau admin");
        return Ok("berhasil. harus role user atau admin");
    }

    [HttpGet, Route("getAdminAndSeller")]
    [Authorize(Roles = "ADMIN")] [Authorize(Roles = StaticUserRoles.SELLER)]
    
    public async Task<IActionResult> GetAdminAndSeller(){
        return Ok("berhasil. harus role admin dan seller");
    }
}
