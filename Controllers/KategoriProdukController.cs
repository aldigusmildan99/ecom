using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ecom.Core.Dtos.KategoriProduk;
using ecom.Core.Dtos.Produk;
using ecom.Core.InterFaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ecom.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class KategoriProdukController : ControllerBase
    {
        private readonly IKategoriProdukServices iKategoriProdukServices;

        public KategoriProdukController(IKategoriProdukServices iKategoriProdukServices)
        {
            this.iKategoriProdukServices = iKategoriProdukServices;
        }

        [HttpGet]
        [Route("")] 
        public async Task<ActionResult> Index([FromQuery]CariKategoriProdukDto cariKategoriProdukDto, [FromQuery]int page=1, [FromQuery] int perPage=10){
            // var data =  await this.iKategoriProdukServices.GetAll();
            var data  = await this.iKategoriProdukServices.Cari(cariKategoriProdukDto, page, perPage);
            return Ok(new ServicesResponseDto(200,true,"success", data));
        }

        [HttpPost]
        public async Task<IActionResult> Store([FromBody] KategoriProdukCreateDto kategoriProdukCreateDto){
           var produk = await this.iKategoriProdukServices.Create(kategoriProdukCreateDto);
           return Ok(new ServicesResponseDto(200,true,"success", produk));
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> Show(int id) {
            var produk = await this.iKategoriProdukServices.GetOne(id);
            return Ok(new ServicesResponseDto(200,true,"success", produk));
        }

        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> delete(int id) {
            var produk = await this.iKategoriProdukServices.Delete(id);
            return Ok(new ServicesResponseDto(200,true,"success", produk));
        }

        [HttpPut]
        [Route("{id}")]
        public async Task<IActionResult> update(int id, KategoriProdukUpdateDto kategoriProdukUpdateDto){
            var produk = await this.iKategoriProdukServices.Update(id, kategoriProdukUpdateDto);

            if(produk == null) return NotFound("Data tidak ditemukan");
            
            return Ok(new ServicesResponseDto(200,true,"success", produk));

        }
    }
}
