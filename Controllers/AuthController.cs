using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;

namespace ecom.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IConfiguration _configuration;

        public AuthController(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager, IConfiguration configuration)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _configuration = configuration;
        }

        [HttpPost]
        [Route("seed-roles")]
        public async Task<IActionResult> SeedRoles(){
            bool isRoleUserExist = await _roleManager.RoleExistsAsync(StaticUserRoles.USER);
            bool isRoleAdminExist = await _roleManager.RoleExistsAsync(StaticUserRoles.ADMIN);
            bool isRoleSellerExist = await _roleManager.RoleExistsAsync(StaticUserRoles.SELLER);

            if(isRoleUserExist && isRoleAdminExist && isRoleSellerExist) {
                return Ok("Roles seeding is already done");
            }

            await _roleManager.CreateAsync(new IdentityRole(StaticUserRoles.USER));
            await _roleManager.CreateAsync(new IdentityRole(StaticUserRoles.ADMIN));
            await _roleManager.CreateAsync(new IdentityRole(StaticUserRoles.SELLER));
            return Ok("Seeded");
        }


        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> Register([FromBody] RegisterDto registerDto)
        {
            var isExistsUser = await _userManager.FindByNameAsync(registerDto.UserName);

            if (isExistsUser != null)
                return BadRequest("UserName Already Exists");

            ApplicationUser newUser = new ApplicationUser()
            {
                FirstName = registerDto.FirstName,
                LastName = registerDto.LastName,
                Email = registerDto.Email,
                UserName = registerDto.UserName,
                SecurityStamp = Guid.NewGuid().ToString(),
            };

            var createUserResult = await _userManager.CreateAsync(newUser, registerDto.Password);

            if (!createUserResult.Succeeded)
            {
                var errorString = "User Creation Failed Beacause: ";
                foreach (var error in createUserResult.Errors)
                {
                    errorString += " # " + error.Description;
                }
                return BadRequest(errorString);
            }

            // Add a Default USER Role to all users
            await _userManager.AddToRoleAsync(newUser, StaticUserRoles.USER);

            return Ok(newUser);
        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody] LoginDto loginDto) {
            var user = await _userManager.FindByNameAsync(loginDto.UserName);

            if(user is null){
                return Unauthorized("Invalid Crederentials");
            }

            var isPasswordCorrect = await _userManager.CheckPasswordAsync(user, loginDto.Password);
            if(!isPasswordCorrect){
                return Unauthorized("Invalid Crederentials");
            }

            var userRoles = await _userManager.GetRolesAsync(user);

            var authClaims = new List<Claim>{
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim(ClaimTypes.NameIdentifier, user.Id),
                new Claim("JWTID", Guid.NewGuid().ToString()),
                new Claim("FirstName", user.FirstName),
                new Claim("LastName", user.LastName),
            };

            foreach (var userRole in userRoles)
            {
                authClaims.Add(new Claim(ClaimTypes.Role, userRole));
            }

            var token  = GenerateNewJWT(authClaims);

            return Ok(token);
        }

        private string GenerateNewJWT(List<Claim> authClaims)
        {
            var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));
            
            var tokenObject = new JwtSecurityToken(
                issuer: _configuration["JWT:ValidIssuer"],
                audience: _configuration["JWT:ValidAudience"],
                claims: authClaims,
                expires: DateTime.Now.AddMinutes(60),
                signingCredentials: new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256)
            );

            var token = new JwtSecurityTokenHandler().WriteToken(tokenObject);

            return token;
        }

        [HttpGet]
        [Route("user-info")]
        [Authorize]
        public async Task<IActionResult> AuthInfo(){
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            return Ok(user);
        }

        [HttpGet]
        [Route("read-claims")]
        [Authorize]
        public async Task<IActionResult> ReadClaims(){
            Request.Headers.TryGetValue("Authorization", out var headerValue);
            var token = headerValue.ToString().Substring(7);
            var fromToken = new JwtSecurityTokenHandler().ReadJwtToken(token);
            var roles2 = fromToken.Claims
            .Where(c => c.Type == ClaimTypes.Role)
            .Select(c => c.Value).ToList();

            var user = User;
            var id = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var fullName = User.FindFirstValue("FirstName") + " " + User.FindFirstValue("LastName");
            var userName = User.Identity.Name;
            var roles = user.FindAll(ClaimTypes.Role).Select(c => c.Value).ToList();
            return Ok(new {
                id,
                userName,
                roles,
                fullName,
                roles2
            });
        }

        [HttpPost]
        [Route("set-as-admin")]
        [Authorize]
        public async Task<IActionResult> SetAsAdmin(){
            var user = await this._userManager.FindByIdAsync(User.FindFirstValue(ClaimTypes.NameIdentifier));
            _ = await this._userManager.AddToRoleAsync(user, StaticUserRoles.ADMIN);
            var temp = new {user.Email, user.UserName};
            return Ok(new ServicesResponseDto(200,true, "set user sebagai admin berhasil. silahlan login kembali", temp));
        }
    
    }
}
