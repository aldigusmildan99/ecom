using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ecom.Core.Dtos.Produk;
using ecom.Core.InterFaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ecom.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ProdukController : ControllerBase
    {
        private readonly IProdukServices iProdukServices;

        public ProdukController(IProdukServices iProdukServices)
        {
            this.iProdukServices = iProdukServices;
        }

        [HttpGet]
        [Route("")] 
        public async Task<ActionResult> Index([FromQuery]CariProdukDto cariProdukDto, [FromQuery]int page=1, [FromQuery] int perPage=10){
            // var data =  await this.iProdukServices.GetAll();
            var data  = await this.iProdukServices.Cari(cariProdukDto, page, perPage);
            return Ok(new ServicesResponseDto(200,true,"success", data));
        }

        [HttpPost]
        public async Task<IActionResult> Store([FromBody] ProdukCreateDto produkCreateDto){
           var produk = await this.iProdukServices.Create(produkCreateDto);
           return Ok(new ServicesResponseDto(200,true,"success", produk));
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> Show(int id) {
            var produk = await this.iProdukServices.GetOne(id);
            return Ok(new ServicesResponseDto(200,true,"success", produk));
        }

        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> delete(int id) {
            var produk = await this.iProdukServices.Delete(id);
            return Ok(new ServicesResponseDto(200,true,"success", produk));
        }

        [HttpPut]
        [Route("{id}")]
        public async Task<IActionResult> update(int id, ProdukUpdateDto produkUpdateDto){
            var produk = await this.iProdukServices.Update(id, produkUpdateDto);

            if(produk == null) return NotFound("Data tidak ditemukan");
            
            return Ok(new ServicesResponseDto(200,true,"success", produk));

        }
    }
}
