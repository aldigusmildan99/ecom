using System.Text.Json.Serialization;
using ecom;
using ecom.Core.Extensions;
using NLog;
using NLog.Web;
using ecom.Core.InterFaces;
using ecom.Core.Services.Produk;
using ecom.Core.Services.KategoriProduk;

// Early init of NLog to allow startup and exception logging, before host is built
var logger = NLog.LogManager.Setup().LoadConfigurationFromAppSettings().GetCurrentClassLogger();
logger.Debug("init main");

var builder = WebApplication.CreateBuilder(args);

 // NLog: Setup NLog for Dependency injection
    builder.Logging.ClearProviders();
    builder.Host.UseNLog();

// Add services to the container.
builder.Services.ConfigureSwager();
builder.Services.ConfigureCors();

builder.Services.AddControllers().AddJsonOptions(options =>
{
    options.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles;
});

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

// Add DB
builder.Services.AddDbContext<ApplicationDbContext>();
// add identity
builder.Services.ConfigureIdentity();
// Add Auth and JwtBearer
builder.Services.ConfigureJwt(builder.Configuration);

    
// injection
builder.Services.AddScoped<IProdukServices, ProdukServices>();
builder.Services.AddScoped<IKategoriProdukServices, KategoriProdukServices>();

var app = builder.Build();
// app.UseCors(builder => builder
//        .AllowAnyHeader()
//        .AllowAnyMethod()
//        .AllowAnyOrigin()
//     );
app.UseCors();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
