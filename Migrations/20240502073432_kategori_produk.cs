﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ecom.Migrations
{
    /// <inheritdoc />
    public partial class kategoriproduk : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "KategoriProdukId",
                table: "produk",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "kategori_produk",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Nama = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_kategori_produk", x => x.Id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateIndex(
                name: "IX_produk_KategoriProdukId",
                table: "produk",
                column: "KategoriProdukId");

            migrationBuilder.AddForeignKey(
                name: "FK_produk_kategori_produk_KategoriProdukId",
                table: "produk",
                column: "KategoriProdukId",
                principalTable: "kategori_produk",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_produk_kategori_produk_KategoriProdukId",
                table: "produk");

            migrationBuilder.DropTable(
                name: "kategori_produk");

            migrationBuilder.DropIndex(
                name: "IX_produk_KategoriProdukId",
                table: "produk");

            migrationBuilder.DropColumn(
                name: "KategoriProdukId",
                table: "produk");
        }
    }
}
