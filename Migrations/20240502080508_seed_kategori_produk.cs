﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ecom.Migrations
{
    /// <inheritdoc />
    public partial class seedkategoriproduk : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            foreach (var item in new[] {"Elektronik","ATK","Rumah Tangga"})
            {
                migrationBuilder.InsertData(
                    table: "kategori_produk",
                    columns: new[] {"Nama"},
                    values: new object[] { item }
                );
            }
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
